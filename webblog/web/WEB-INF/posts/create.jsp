<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create a post</title>
</head>
<body>
<% if (request.getAttribute("errors") != null) { %>
<b>Some errors occurred:</b>
<% for (Map.Entry<String, String> entry : ((HashMap<String, String>) request.getAttribute("errors")).entrySet()) {%>
<div class="alert alert-danger">
    <% out.print(entry.getValue()); %>
</div>
<% } %>
<% } %>
<div class="panel panel-default">
    <form action="/create-post" method="post">

        <div class="panel-heading">
            <h2>Create Post</h2>
        </div>
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="tittle">Tittle</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" class="form-control" id="tittle"
                               placeholder="Enter Tittle">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="post">Post</label>
                    <div class="col-sm-10">
                        <textarea rows = "5" cols="15" type="text" name="body" class="form-control" id="post"
                               placeholder="Enter Post">
                        </textarea>
                    </div>
                </div>

                <div class="pickVisibility">
                    <label class="control-label col-sm-2" for="post">Visibility</label>
                    <div class="col-sm-10">
                        <select name="visibility">
                            <option value="1">Public</option>
                            <option value="2">Friends only</option>
                            <option value="3">Private</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" value="createPost" class="btn btn-default">CreatePost</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </form>
</div>
</body>
</html>
