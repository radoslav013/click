package Servlets;

import Helpers.Database;
import Models.FriendsList;
import Models.Post;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "Servlets.PostsListServlet", urlPatterns = {"/drama"})
public class PostsListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            Connection connection = Database.getConnection();
            PreparedStatement ps = null;
            ResultSet rs = null;
            ArrayList<Post> posts = new ArrayList<>();

            ps = connection.prepareStatement("Select * From post where visibility = 1");
            resultSetData(posts, rs, ps);

            HttpSession session = request.getSession();
            if (request.getSession().getAttribute("id") != null) {
                int userId = (int) request.getSession().getAttribute("id");
                ps = connection.prepareStatement("SELECT * FROM post WHERE visibility=3 AND user_id=" + userId);
                resultSetData(posts, rs, ps);
                ps = connection.prepareStatement("SELECT * from post inner join friends on ((post.user_id = friends.from_id AND friends.accepted = 1) or (post.user_id = friends.to_id AND friends.accepted = 1)) AND (from_id=" + userId + " or to_id=" + userId + ") AND post.visibility = 2");
                resultSetData(posts, rs, ps);
            }
            request.setAttribute("posts", posts);
            getServletContext().getRequestDispatcher("/WEB-INF/posts/lists.jsp").forward(request, response);


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void resultSetData(ArrayList<Post> posts, ResultSet rs, PreparedStatement ps) throws SQLException {
        rs = ps.executeQuery();
        while (rs.next()) {
            Post post = new Post(
                    rs.getInt("id"),
                    rs.getString("title"),
                    rs.getString("body"),
                    rs.getDate("date"),
                    rs.getInt("visibility"),
                    rs.getInt("user_id")
            );
            posts.add(post);
        }
    }
}
